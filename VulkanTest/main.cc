/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <cstdlib>
#include <windows.h>
#include <GLFW/glfw3.h>
#include <Entropy/Application.hh>
#include <glm/glm.hpp>
#include "Report.hh"
#include "CommandPool.hh"

using namespace std;
using namespace Entropy::Vulkan;

// 2018-09-17 AMR TODO: support resizing
const int WIDTH = 800;
const int HEIGHT = 600;

Entropy::Log::Logger AppLog("Application");

// 2018-09-17 AMR NOTE: this might be a good way to do this/this might be a terrible way to do this
// 2018-09-17 AMR TODO: make wrapper types in Pipeline::VertexInput
struct Vertex
{
	glm::vec2 position;
	glm::vec3 color;

	static vector<VkVertexInputBindingDescription> getBinding()
	{
		vector<VkVertexInputBindingDescription> ret(1);

		ret[0].binding = 0;
		ret[0].stride = sizeof(Vertex);
		ret[0].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

		return ret;
	}
	// 2018-09-17 AMR TODO: support other contigous array types
	static vector<VkVertexInputAttributeDescription> getAttributes()
	{
		vector<VkVertexInputAttributeDescription> ret(2);

		ret[0].binding = 0;
		ret[0].location = 0;
		ret[0].format = VK_FORMAT_R32G32_SFLOAT;
		ret[0].offset = offsetof(Vertex, position);

		ret[1].binding = 0;
		ret[1].location = 1;
		ret[1].format = VK_FORMAT_R32G32B32_SFLOAT;
		ret[1].offset = offsetof(Vertex, color);

		return ret;
	}
};

class Application :
	public Entropy::Application
{
	public:
		Application();
		~Application();
		void operator () ();
		void Draw();
	private:
		shared_ptr<Instance> _instance;
		shared_ptr<Surface> _surface;
		shared_ptr<Report> _report;
		shared_ptr<PhysicalDevice> _phys;
		shared_ptr<Device> _dev;
		shared_ptr<SwapChain> _chain;
		shared_ptr<RenderPass> _render;
		shared_ptr<DescriptorSetLayout> _layout;
		shared_ptr<Pipeline> _pipeline;
		shared_ptr<Buffer> _buffer;
		vector<shared_ptr<Framebuffer>> _framebuffers;
		shared_ptr<CommandPool> _graphics;
		shared_ptr<Queue> _present;
		vector<CommandBuffer, CommandPool::allocator_type> _buffers;
		vector<shared_ptr<Semaphore>> _sem_image;
		vector<shared_ptr<Semaphore>> _sem_render;
		vector<shared_ptr<Fence>> _fence;
		GLFWwindow *_window;
		const vector<Vertex> vertices = {
			{
				{0.0f, -0.5f},
				{1.0f, 0.0f, 0.0f},
			},
			{
				{0.5f, 0.5f},
				{0.0f, 1.0f, 0.0f},
			},
			{
				{-0.5f, 0.5f},
				{0.0f, 0.0f, 1.0f},
			},
		};
};

int APIENTRY WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	try
	{
		::Application app;
		app();

		return EXIT_SUCCESS;
	}
	catch (exception &e)
	{
		MessageBox(nullptr, e.what(), "Error!", MB_ICONEXCLAMATION | MB_OK);
	}

	return EXIT_FAILURE;
}

void ::Application::operator() ()
{
	while(!glfwWindowShouldClose(_window)) {
		glfwPollEvents();
		Draw();
	}
}

size_t cur = 0;

// 2018-09-17 AMR TODO: recreate swapchain
// 2018-09-17 AMR TODO: also handle minimizing
void ::Application::Draw()
{
	_fence[cur]->Wait();
	_fence[cur]->Reset();
	uint32_t idx = _chain->NextIndex(*_sem_image[cur]);

	_buffers[idx].Submit(
		_fence[idx],
		{
			_sem_image[idx],
		},
		{
			_sem_render[idx],
		}
	);

	_framebuffers[idx]->Present(_present);

	cur = (idx + 1) % _framebuffers.size();
}

::Application::Application() :
	Entropy::Application()
{
	addFileLog("vulkan-test-%Y-%m-%d-%H-%M-%S.%N.log"s);

	if (!glfwInit())
		ENTROPY_THROW(Exception("Failed to initialize glfw"));

	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

	_instance = make_shared<Instance>();
	_window = glfwCreateWindow(WIDTH, HEIGHT, "Vulkan Test", nullptr, nullptr);
	_surface = make_shared<Surface>(_instance, _window);

#	ifdef DEBUG
		_report = make_shared<Report>(_instance, [](const auto &d) {
			ENTROPY_LOG(AppLog, d.LogSeverity()) << d.Message();
		});
#	endif

	// 2018-09-04 AMR TODO: Clean this up and DRY
	_phys = PhysicalDevice::Get(_instance, [this](const auto &d) -> int {
		if(
			!d->hasExtension(VK_KHR_SWAPCHAIN_EXTENSION_NAME) ||
			d->Formats(_surface).empty() ||
			d->Modes(_surface).empty()
		) {
			return 0;
		}

		int i, g, p;
		i = 0;
		g = p = -1;
		for(const auto &q : d->Queues()) {
			if(q.Count() > 0 && q.Flags() & VK_QUEUE_GRAPHICS_BIT)
				g = i;
			if(q.Count() > 0 && q.canPresent(_surface))
				p = i;

			if(g != -1 && p != -1)
				break;

			i++;
		}

		if(p == -1 || g == -1)
			return 0;

		return
			((d->Properties().deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) ? 1000 : 0) +
			d->Properties().limits.maxImageDimension2D
		;
	});

	// 2018-09-22 AMR FIXME: DRY
	{
		vector<QueueFamily> queues;
		bool hasG, hasP;
		hasG = hasP = false;
		for(const auto &q : _phys->Queues()) {
			if(q.Count() > 0 && q.Flags() & VK_QUEUE_GRAPHICS_BIT && !hasG) {
				queues.emplace_back(q);
				hasG = true;
			}
			if(q.Count() > 0 && q.canPresent(_surface) && !hasP) {
				if(!hasG || queues.back().Index() != q.Index())
					queues.emplace_back(q);
				hasP = true;
			}
			if(hasG && hasP)
				break;
		}
		vector<const char *> extensions = {
			VK_KHR_SWAPCHAIN_EXTENSION_NAME
		};
		_dev = make_shared<Device>(_phys, queues, extensions);
	}

	_chain = make_shared<SwapChain>(_dev, _surface);
	_render = make_shared<RenderPass>(
		_dev,
		vector<RenderPass::Attachment>{
			RenderPass::Attachment(
				_chain->Format(),
				VK_ATTACHMENT_LOAD_OP_CLEAR,
				VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
			)
		},
		vector<RenderPass::Subpass>{
			RenderPass::Subpass(
				VK_PIPELINE_BIND_POINT_GRAPHICS,
				{},
				{
					{
						0,
						VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
					},
				}
			)
		}
	);
	_layout = make_shared<DescriptorSetLayout>(
		_dev,
		vector<DescriptorSetLayout::Binding>{
			DescriptorSetLayout::Binding(0, VK_SHADER_STAGE_VERTEX_BIT)
		}
	);
	_pipeline = make_shared<Pipeline>(
		_dev,
		Shader(
			"main",
			make_shared<ShaderModule>(
				_dev,
				"./shader.vert.spv"s
			),
			make_shared<ShaderModule>(
				_dev,
				"./shader.frag.spv"s
			)
		),
		_render,
		Pipeline::VertexInput(Vertex::getBinding(), Vertex::getAttributes()),
		Pipeline::InputAssembly(),
		Pipeline::Viewport(_chain->Extent()),
		Pipeline::Rasterization(),
		Pipeline::Multisample(),
		Pipeline::ColorBlend(),
		vector<shared_ptr<DescriptorSetLayout>>{
			_layout,
		}
	);
	_buffer = make_shared<Buffer>(
		_dev,
		sizeof(Vertex) * vertices.size(),
		VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,	// 2018-11-02 AMR TODO: wrap these values
		[this](const auto t, const auto p) {
			auto props = _phys->Memory();

			for(auto x = 0u; x < props.memoryTypeCount; x++)
				if(t & (1 << x) && (props.memoryTypes[x].propertyFlags & p) == p)
					return x;

			ENTROPY_THROW(Exception("Failed to find suitable memory"));
		}
	);
	_buffer->Write(vertices);
	_framebuffers = Framebuffer::Get(_chain, _render);
	for(const auto &q : _dev->Queues()) {
		if(!_graphics && q->Family().Flags() & VK_QUEUE_GRAPHICS_BIT) {
			_graphics = make_shared<CommandPool>(_dev, q);
		}
		if(!_present && q->Family().canPresent(_surface)) {
			_present = q;
		}
	}

	_sem_image.reserve(_framebuffers.size());
	_sem_render.reserve(_framebuffers.size());
	_fence.reserve(_framebuffers.size());

	for(int x = 0; x < _framebuffers.size(); x++) {
		_sem_image.emplace_back(make_shared<Semaphore>(_dev));
		_sem_render.emplace_back(make_shared<Semaphore>(_dev));
		_fence.emplace_back(make_shared<Fence>(_dev));
	}

	if(!_graphics)
		ENTROPY_THROW(Exception("Failed to find graphics command queue"));
	_buffers = _graphics->get<>();
	_buffers.resize(_framebuffers.size());

	for(auto x = 0; x < _framebuffers.size(); x++) {
		_buffers[x].Begin();
		_buffers[x].BeginRender(_framebuffers[x]);
		_buffers[x].Bind(_pipeline);
		_buffers[x].Bind(_buffer);
		_buffers[x].Draw(static_cast<uint32_t>(vertices.size()), 1, 0, 0);
		_buffers[x].EndRender();
		_buffers[x].End();
	}
}

::Application::~Application()
{
	glfwDestroyWindow(_window);
	glfwTerminate();
}
	