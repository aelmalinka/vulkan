/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Pipeline.hh"

using namespace Entropy::Vulkan;
using namespace std;

Pipeline::Multisample::Multisample() :
	_info{
		VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
		nullptr,
		{},
		VK_SAMPLE_COUNT_1_BIT,
		VK_FALSE,
		0.0f,
		nullptr,
		VK_FALSE,
		VK_FALSE
	}
{}

const VkPipelineMultisampleStateCreateInfo &Pipeline::Multisample::Info() const
{
	return _info;
}
