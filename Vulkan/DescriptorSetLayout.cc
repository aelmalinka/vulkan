/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "DescriptorSetLayout.hh"

using namespace Entropy::Vulkan;
using namespace std;

DescriptorSetLayout::DescriptorSetLayout(const shared_ptr<Device> &device, const std::vector<Binding> &bindings) :
	_device(device),
	_handle(VK_NULL_HANDLE)
{
	vector<VkDescriptorSetLayoutBinding> b(bindings.size());
	b.reserve(bindings.size());

	for(auto &t : bindings) {
		b.emplace_back(t.Value());
	}

	VkDescriptorSetLayoutCreateInfo create = {};
	create.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	create.bindingCount = static_cast<uint32_t>(b.size());
	create.pBindings = b.data();

	EVK_SUCCESS_OR_THROW(
		vkCreateDescriptorSetLayout(_device->Handle(), &create, nullptr, &_handle),
		"Failed to create DescriptorSetLayout"
	);
}

DescriptorSetLayout::~DescriptorSetLayout()
{
	vkDestroyDescriptorSetLayout(_device->Handle(), _handle, nullptr);
}

const VkDescriptorSetLayout &DescriptorSetLayout::Handle() const
{
	return _handle;
}

DescriptorSetLayout::Binding::Binding(
	const uint32_t binding,
	const VkShaderStageFlags flags,
	const uint32_t count,
	const VkDescriptorType type
) :
	_value()
{
	_value.binding = binding;
	_value.stageFlags = flags;
	_value.descriptorCount = count;
	_value.descriptorType = type;
}

const VkDescriptorSetLayoutBinding &DescriptorSetLayout::Binding::Value() const
{
	return _value;
}
