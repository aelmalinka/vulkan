/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Pipeline.hh"

using namespace Entropy::Vulkan;
using namespace std;

Pipeline::Layout::Layout(
	const shared_ptr<Device> &device,
	const vector<shared_ptr<DescriptorSetLayout>> &layouts
) :
	_device(device),
	_layouts(layouts),
	_handle(VK_NULL_HANDLE)
{
	vector<VkDescriptorSetLayout> l;
	l.reserve(_layouts.size());

	for(auto &t : layouts) {
		l.emplace_back(t->Handle());
	}

	VkPipelineLayoutCreateInfo create = {};
	create.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	create.setLayoutCount = static_cast<uint32_t>(_layouts.size());
	create.pSetLayouts = l.data();

	EVK_SUCCESS_OR_THROW(
		vkCreatePipelineLayout(_device->Handle(), &create, nullptr, &_handle),
		"Failed to create Pipeline Layout"
	);
}

Pipeline::Layout::~Layout()
{
	vkDestroyPipelineLayout(_device->Handle(), _handle, nullptr);
}

const VkPipelineLayout &Pipeline::Layout::Handle() const
{
	return _handle;
}
