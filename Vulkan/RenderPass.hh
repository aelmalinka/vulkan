/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_VULKAN_RENDERPASS_INC
#	define ENTROPY_VULKAN_RENDERPASS_INC

#	include "Device.hh"

	namespace Entropy
	{
		namespace Vulkan
		{
			class RenderPass
			{
				public:
					class Attachment;
					class Subpass;
				public:
					RenderPass(
						const std::shared_ptr<Device> &,
						const std::vector<Attachment> &,
						const std::vector<Subpass> &
					);
					~RenderPass();
					const VkRenderPass &Handle() const;
				private:
					std::shared_ptr<Device> _device;
					VkRenderPass _handle;
				public:
					class Attachment
					{
						public:
							Attachment(
								const VkFormat,
								const VkAttachmentLoadOp,
								const VkImageLayout,
								const VkSampleCountFlagBits = VK_SAMPLE_COUNT_1_BIT,
								const VkAttachmentStoreOp = VK_ATTACHMENT_STORE_OP_STORE,
								const VkImageLayout = VK_IMAGE_LAYOUT_UNDEFINED,
								const VkAttachmentLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
								const VkAttachmentStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE
							);
							const VkAttachmentDescription &Value() const;
						private:
							VkAttachmentDescription _value;
					};
					class Subpass
					{
						public:
							// 2018-09-08 AMR TODO: Stencil/Depth, Perserve, and Resolve attachments
							explicit Subpass(
								const VkPipelineBindPoint,
								const std::vector<VkAttachmentReference> & = {},
								const std::vector<VkAttachmentReference> & = {}
							);
							const VkSubpassDescription &Value() const;
						private:
							std::vector<VkAttachmentReference> _input;
							std::vector<VkAttachmentReference> _color;
							VkSubpassDescription _value;
					};
			};
		}
	}

#endif
