/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Pipeline.hh"

using namespace Entropy::Vulkan;
using namespace std;

Pipeline::VertexInput::VertexInput(
	const vector<VkVertexInputBindingDescription> &binds,
	const vector<VkVertexInputAttributeDescription> &attribs
) :
	_info()
{
	_info.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;

	_info.vertexBindingDescriptionCount = static_cast<uint32_t>(binds.size());
	_info.vertexAttributeDescriptionCount = static_cast<uint32_t>(attribs.size());

	_info.pVertexBindingDescriptions = binds.data();
	_info.pVertexAttributeDescriptions = attribs.data();
}

const VkPipelineVertexInputStateCreateInfo &Pipeline::VertexInput::Info() const
{
	return _info;
}
