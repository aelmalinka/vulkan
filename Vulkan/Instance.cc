/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Instance.hh"

using namespace Entropy::Vulkan;
using namespace std;

Instance::Instance() :
	Instance(ApplicationInfo(
		"Default Application",
		ApplicationInfo::Version,
		{
			// 2018-08-28 AMR TODO: multiple platforms more cleanly config.h?
			"VK_KHR_surface",
#			ifdef _WIN32
				"VK_KHR_win32_surface",
#			else
				"VK_KHR_xcb_surface",
#			endif
		},
		{
#			ifdef DEBUG
				VK_EXT_DEBUG_UTILS_EXTENSION_NAME,
#			endif
		},
		{},
		{
#			ifdef DEBUG
				"VK_LAYER_LUNARG_standard_validation",
#			endif
		}
	))
{}

Instance::Instance(ApplicationInfo &&info) :
	_handle(),
	_info(move(info))
{
	VkInstanceCreateInfo create = {};

	create.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	create.pApplicationInfo = &_info.Handle();

	create.enabledExtensionCount = static_cast<uint32_t>(_info.Extensions().size());
	create.ppEnabledExtensionNames = _info.Extensions().data();

	create.enabledLayerCount = static_cast<uint32_t>(_info.Layers().size());
	create.ppEnabledLayerNames = _info.Layers().data();

	EVK_SUCCESS_OR_THROW(vkCreateInstance(&create, nullptr, &_handle), "Failed to create Instance");
}

Instance::~Instance()
{
	vkDestroyInstance(_handle, nullptr);
}

const VkInstance &Instance::Handle() const
{
	return _handle;
}

const ApplicationInfo &Instance::Info() const
{
	return _info;
}
