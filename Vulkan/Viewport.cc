/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Pipeline.hh"

using namespace Entropy::Vulkan;
using namespace std;

Pipeline::Viewport::Viewport(
	const VkExtent2D &ext,
	const VkOffset2D &off,
	const float min,
	const float max
) :
	Viewport(ext, ext, off, off, min, max)
{}

Pipeline::Viewport::Viewport(
	const VkExtent2D &vext,
	const VkExtent2D &sext,
	const VkOffset2D &voff,
	const VkOffset2D &soff,
	const float min,
	const float max
) :
	_info(),
	_view{
		static_cast<float>(voff.x),
		static_cast<float>(voff.y),
		static_cast<float>(vext.width),
		static_cast<float>(vext.height),
		min,
		max
	},
	_scissor{
		soff,
		sext
	}
{
	_info.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	_info.viewportCount = 1;
	_info.pViewports = &_view;
	_info.scissorCount = 1;
	_info.pScissors = &_scissor;
}

const VkPipelineViewportStateCreateInfo &Pipeline::Viewport::Info() const
{
	return _info;
}
