/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "PhysicalDevice.hh"
#include <map>
#include <utility>

using namespace Entropy::Vulkan;
using namespace std;

PhysicalDevice::PhysicalDevice(const shared_ptr<Instance> &instance, VkPhysicalDevice handle) :
	enable_shared_from_this(),
	_instance(instance),
	_handle(handle),
	_properties(),
	_features(),
	_memory(),
	_queues(),
	_caps(),
	_formats(),
	_modes(),
	_extensions()
{
	vkGetPhysicalDeviceProperties(_handle, &_properties);
	vkGetPhysicalDeviceFeatures(_handle, &_features);
	vkGetPhysicalDeviceMemoryProperties(_handle, &_memory);

	uint32_t count = 0;
	vkEnumerateDeviceExtensionProperties(_handle, nullptr, &count, nullptr);
	vector<VkExtensionProperties> extensions(count);
	vkEnumerateDeviceExtensionProperties(_handle, nullptr, &count, extensions.data());

	// 2018-09-04 AMR TODO: do we want to store more than just name?
	for(const auto &e : extensions) {
		_extensions.insert(e.extensionName);
	}
}

const VkPhysicalDevice &PhysicalDevice::Handle() const
{
	return _handle;
}

const shared_ptr<Instance> &PhysicalDevice::getInstance() const
{
	return _instance;
}

const VkPhysicalDeviceProperties &PhysicalDevice::Properties() const
{
	return _properties;
}

const VkPhysicalDeviceFeatures &PhysicalDevice::Features() const
{
	return _features;
}

const VkPhysicalDeviceMemoryProperties &PhysicalDevice::Memory() const
{
	return _memory;
}

const vector<QueueFamily> &PhysicalDevice::Queues()
{
	if(_queues.empty()) {
		vector<VkQueueFamilyProperties> queues;
		uint32_t cnt = 0;
		vkGetPhysicalDeviceQueueFamilyProperties(_handle, &cnt, nullptr);
		queues.resize(cnt);
		_queues.reserve(cnt);
		vkGetPhysicalDeviceQueueFamilyProperties(_handle, &cnt, queues.data());

		auto i = 0u;
		for(const auto &q : queues) {
			_queues.emplace_back(shared_from_this(), i++, q);
		}
	}

	return _queues;
}

const Surface::Capabilities &PhysicalDevice::Capabilites(const shared_ptr<Surface> &s)
{
	// 2018-09-04 AMR TODO: compare surfaces
	if(!_caps || _caps->getSurface().get() != s.get()) {
		_caps = make_shared<Surface::Capabilities>(shared_from_this(), s);
		_formats = Surface::Format::Get(shared_from_this(), s);
		_modes = Surface::PresentMode::Get(shared_from_this(), s);
	}

	return *_caps;
}

const vector<Surface::Format> &PhysicalDevice::Formats(const shared_ptr<Surface> &s)
{
	// 2018-09-04 AMR TODO: compare surfaces
	if(!_caps || _caps->getSurface().get() != s.get()) {
		_caps = make_shared<Surface::Capabilities>(shared_from_this(), s);
		_formats = Surface::Format::Get(shared_from_this(), s);
		_modes = Surface::PresentMode::Get(shared_from_this(), s);
	}

	return _formats;
}

const vector<Surface::PresentMode> &PhysicalDevice::Modes(const shared_ptr<Surface> &s)
{
	// 2018-09-04 AMR TODO: compare surfaces
	if(!_caps || _caps->getSurface().get() != s.get()) {
		_caps = make_shared<Surface::Capabilities>(shared_from_this(), s);
		_formats = Surface::Format::Get(shared_from_this(), s);
		_modes = Surface::PresentMode::Get(shared_from_this(), s);
	}

	return _modes;
}

bool PhysicalDevice::hasExtension(const string &name) const
{
	return _extensions.find(name) != _extensions.end();
}

shared_ptr<PhysicalDevice> PhysicalDevice::Get(const shared_ptr<Instance> &instance, const function<int(const shared_ptr<PhysicalDevice> &)> &f)
{
	uint32_t cnt = 0;
	vector<VkPhysicalDevice> devices;

	vkEnumeratePhysicalDevices(instance->Handle(), &cnt, nullptr);

	if(cnt == 0)
		ENTROPY_THROW(Exception("No Graphics Adapters support Vulkan"));

	devices.resize(cnt);
	vkEnumeratePhysicalDevices(instance->Handle(), &cnt, devices.data());

	multimap<int, shared_ptr<PhysicalDevice>> chosen;
	for(const auto &d : devices) {
		auto device = make_shared<PhysicalDevice>(instance, d);
		chosen.insert(make_pair(f(device), device));
	}

	if(!chosen.empty() && chosen.rbegin()->first > 0)
		return chosen.rbegin()->second;
	else
		ENTROPY_THROW(Exception("Failed to find a suitable Graphics Adapater"));
}
