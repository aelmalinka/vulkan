/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Buffer.hh"

using namespace Entropy::Vulkan;
using namespace std;

Buffer::Buffer(
	const shared_ptr<Device> &device,
	const VkDeviceSize sz,
	const VkBufferUsageFlags usage,
	const function<uint32_t(const uint32_t, const VkMemoryPropertyFlags)> &cb
) :
	_device(device),
	_handle(VK_NULL_HANDLE),
	_memory(VK_NULL_HANDLE),
	_sz(sz)
{
	VkBufferCreateInfo create = {};
	create.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;

	create.size = _sz;
	create.usage = usage;
	create.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	EVK_SUCCESS_OR_THROW(
		vkCreateBuffer(_device->Handle(), &create, nullptr, &_handle),
		"Failed to create Buffer"
	);

	VkMemoryRequirements req;
	vkGetBufferMemoryRequirements(_device->Handle(), _handle, &req);

	VkMemoryAllocateInfo alloc = {};
	alloc.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	alloc.allocationSize = req.size;
	alloc.memoryTypeIndex = cb(
		req.memoryTypeBits,
		VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT
	);

	EVK_SUCCESS_OR_THROW(
		vkAllocateMemory(_device->Handle(), &alloc, nullptr, &_memory),
		"Failed to allocate memory for buffer"
	);
	EVK_SUCCESS_OR_THROW(
		vkBindBufferMemory(_device->Handle(), _handle, _memory, 0),
		"Failed to bind memory to buffer"
	);
}

Buffer::~Buffer()
{
	vkDestroyBuffer(_device->Handle(), _handle, nullptr);
	vkFreeMemory(_device->Handle(), _memory, nullptr);
}

const VkBuffer &Buffer::Handle() const
{
	return _handle;
}
