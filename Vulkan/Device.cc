/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Device.hh"

using namespace Entropy::Vulkan;
using namespace std;

Device::Device(const shared_ptr<PhysicalDevice> &phys, const vector<QueueFamily> &indices, const vector<const char *> &extensions) :
	enable_shared_from_this(),
	_physical(phys),
	_handle(VK_NULL_HANDLE),
	_queues()
{
	VkDeviceCreateInfo create = {};
	VkPhysicalDeviceFeatures features = {};

	auto x = 0u;
	vector<VkDeviceQueueCreateInfo> queues(indices.size());

	float pri = 1.0f;
	for(const auto &q : indices) {
		queues[x].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queues[x].queueFamilyIndex = q.Index();
		queues[x].pQueuePriorities = &pri;
		queues[x++].queueCount = 1;
	}

	create.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	create.pQueueCreateInfos = queues.data();
	create.queueCreateInfoCount = static_cast<uint32_t>(queues.size());
	create.pEnabledFeatures = &features;

	create.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
	create.ppEnabledExtensionNames = extensions.data();
	create.enabledLayerCount = static_cast<uint32_t>(phys->getInstance()->Info().Layers().size());
	create.ppEnabledLayerNames = phys->getInstance()->Info().Layers().data();

	EVK_SUCCESS_OR_THROW(vkCreateDevice(phys->Handle(), &create, nullptr, &_handle), "Failed to create Device");

	x = 0u;
	for(const auto &q : indices) {
		VkQueue t = VK_NULL_HANDLE;
		vkGetDeviceQueue(_handle, q.Index(), x++, &t);
		if(t == VK_NULL_HANDLE)
			ENTROPY_THROW(Exception("Invalid Queue returned"));
		_queues.emplace_back(make_shared<Queue>(t, _physical->Queues()[q.Index()], q.Index()));
	}
}

Device::~Device()
{
	Wait();
	vkDestroyDevice(_handle, nullptr);
}

const shared_ptr<PhysicalDevice> &Device::getPhysical() const
{
	return _physical;
}

const VkDevice &Device::Handle() const
{
	return _handle;
}

vector<shared_ptr<Queue>> &Device::Queues()
{
	return _queues;
}

void Device::Wait() const
{
	EVK_SUCCESS_OR_THROW(
		vkDeviceWaitIdle(_handle),
		"Failed to wait for device"
	);
}
