/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_VULKAN_COMMANDPOOL_IMPL
#	define ENTROPY_VULKAN_COMMANDPOOL_IMPL

#	include "CommandPool.hh"

	namespace Entropy
	{
		namespace Vulkan
		{
			template<typename C>
			C CommandPool::get(const VkCommandBufferLevel lvl)
			{
				_allocs.push_back(Allocator<CommandBuffer>(shared_from_this(), lvl));
				C r(std::any_cast<Allocator<CommandBuffer>>(_allocs.back()));
				return r;
			}

			template<typename T>
			CommandPool::Allocator<T>::Allocator() noexcept :
				_pool(),
				_level(VK_COMMAND_BUFFER_LEVEL_PRIMARY)
			{}

			template<typename T>
			CommandPool::Allocator<T>::Allocator(const std::shared_ptr<CommandPool> &p, const VkCommandBufferLevel l) noexcept :
				_pool(p),
				_level(l)
			{}

			template<typename T>
			template<typename U>
			CommandPool::Allocator<T>::Allocator(const Allocator<U> &o) noexcept :
				_pool(o.getPool()),
				_level(o.getLevel())
			{}

			template<typename T>
			const std::shared_ptr<CommandPool> &CommandPool::Allocator<T>::getPool() const
			{
				return _pool;
			}

			template<typename T>
			const VkCommandBufferLevel &CommandPool::Allocator<T>::getLevel() const
			{
				return _level;
			}

			template<typename T>
			T *CommandPool::Allocator<T>::allocate(std::size_t n)
			{
				return static_cast<T *>(::operator new(n * sizeof(T)));
			}

			template<typename T>
			void CommandPool::Allocator<T>::deallocate(T *p, std::size_t) noexcept
			{
				::operator delete(p);
			}

			template<typename T>
			template<typename U, typename ...Args>
			void CommandPool::Allocator<T>::construct(U *p, Args && ...args)
			{
				::new(p) U(std::forward<Args>(args)...);
			}

			template<typename T>
			template<typename ...Args>
			void CommandPool::Allocator<T>::construct(CommandBuffer *p, Args && ...)
			{
				if(!_pool)
					ENTROPY_THROW(Exception("Attempting to allocate CommandBuffer on an invalid CommandPool"));

				VkCommandBuffer t;

				VkCommandBufferAllocateInfo info = {};
				info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
				info.commandPool = _pool->Handle();
				info.commandBufferCount = 1;
				info.level = _level;

				EVK_SUCCESS_OR_THROW(
					vkAllocateCommandBuffers(_pool->getDevice()->Handle(), &info, &t),
					"Failed to allocate command buffer"
				);

				::new(p) CommandBuffer(_pool, t);
			}

			template<typename T, typename U>
			bool operator == (const CommandPool::Allocator<T> &a, const CommandPool::Allocator<U> &b) noexcept
			{
				return
					a.getPool() == b.getPool() &&
					a.getLevel() == b.getLevel()
				;
			}

			template<typename T, typename U>
			bool operator != (const CommandPool::Allocator<T> &a, const CommandPool::Allocator<U> &b) noexcept
			{
				return
					a.getPool() != b.getPool() ||
					a.getLevel() != b.getLevel()
				;
			}
		}
	}

#endif
