/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_VULKAN_QUEUE_INC
#	define ENTROPY_VULKAN_QUEUE_INC

#	include "Device.hh"

	namespace Entropy
	{
		namespace Vulkan
		{
			class Queue
			{
				public:
					Queue(const VkQueue, const QueueFamily &, const std::uint32_t);
					const VkQueue &Handle() const;
					const std::uint32_t &Index() const;
					const QueueFamily &Family() const;
				private:
					VkQueue _handle;
					const QueueFamily &_family;
					std::uint32_t _index;
			};
		}
	}

#endif
