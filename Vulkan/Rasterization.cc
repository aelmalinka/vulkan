/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Pipeline.hh"

using namespace Entropy::Vulkan;
using namespace std;

Pipeline::Rasterization::Rasterization() :
	_info()
{
	_info.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	_info.depthClampEnable = VK_FALSE;
	_info.polygonMode = VK_POLYGON_MODE_FILL;
	_info.lineWidth = 1.0f;
	_info.cullMode = VK_CULL_MODE_BACK_BIT;
	_info.frontFace = VK_FRONT_FACE_CLOCKWISE;
}

const VkPipelineRasterizationStateCreateInfo &Pipeline::Rasterization::Info() const
{
	return _info;
}
