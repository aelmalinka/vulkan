/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_VULKAN_INSTANCE_INC
#	define ENTROPY_VULKAN_INSTANCE_INC

#	include "ApplicationInfo.hh"

	namespace Entropy
	{
		namespace Vulkan
		{
			class Instance
			{
				public:
					Instance();
					explicit Instance(ApplicationInfo &&);
					~Instance();
					const VkInstance &Handle() const;
					const ApplicationInfo &Info() const;
				private:
					VkInstance _handle;
					ApplicationInfo _info;
			};
		}
	}

#endif
