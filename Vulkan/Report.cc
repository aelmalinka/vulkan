/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Report.hh"

using namespace Entropy::Vulkan;
using namespace std;

using Entropy::Vulkan::Events::Debug;

static VKAPI_ATTR VkBool32 VKAPI_CALL callback(
	VkDebugUtilsMessageSeverityFlagBitsEXT,
	VkDebugUtilsMessageTypeFlagsEXT,
	const VkDebugUtilsMessengerCallbackDataEXT *,
	void *
);

Report::Report(const shared_ptr<Instance> &instance, const function<void(const Debug &)> &cb) :
	SharedData<detail::calls>(instance),
	_instance(instance),
	_handle(),
	_cb(cb)
{
	VkDebugUtilsMessengerCreateInfoEXT create = {};

	create.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
	create.messageSeverity =
		VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
		VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT |
		VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
		VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT
	;
	create.messageType =
		VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
		VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
		VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT
	;
	create.pfnUserCallback = callback;
	create.pUserData = &_cb;

	EVK_SUCCESS_OR_THROW(
		shared()->Create(_instance->Handle(), &create, nullptr, &_handle),
		"Failed to create Report Handler"
	);
}

Report::~Report()
{
	shared()->Destroy(_instance->Handle(), _handle, nullptr);
}

static VKAPI_ATTR VkBool32 VKAPI_CALL callback(
	VkDebugUtilsMessageSeverityFlagBitsEXT sev,
	VkDebugUtilsMessageTypeFlagsEXT type,
	const VkDebugUtilsMessengerCallbackDataEXT *data,
	void *user
) {
	function<void(const Debug &)> *f = reinterpret_cast<function<void(const Debug &)> *>(user);

	if (!f || !user || !data) {
		ENTROPY_LOG(Log, Severity::Critical) << "Report Callback and No Userdata Pointer or Data Pointer";
		// 2018-08-28 AMR XXX: true means abort call (validation layers)
		return VK_FALSE;
	}

	(*f)(Debug(sev, type, data));

	return VK_FALSE;
}

using namespace Entropy::Vulkan::detail;

calls::calls(const shared_ptr<Instance> &instance) :
	_create(nullptr),
	_destroy(nullptr)
{
	_create = reinterpret_cast<PFN_vkCreateDebugUtilsMessengerEXT>(
		vkGetInstanceProcAddr(instance->Handle(), "vkCreateDebugUtilsMessengerEXT")
	);
	_destroy = reinterpret_cast<PFN_vkDestroyDebugUtilsMessengerEXT>(
		vkGetInstanceProcAddr(instance->Handle(), "vkDestroyDebugUtilsMessengerEXT")
	);
}

VkResult calls::Create(
	VkInstance instance,
	const VkDebugUtilsMessengerCreateInfoEXT *info,
	const VkAllocationCallbacks *cbs,
	VkDebugUtilsMessengerEXT *handle
) const {
	return _create(instance, info, cbs, handle);
}

void calls::Destroy(
	VkInstance instance,
	VkDebugUtilsMessengerEXT handle,
	const VkAllocationCallbacks *cbs
) const {
	_destroy(instance, handle, cbs);
}
