/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_VULKAN_DESCRIPTORSETLAYOUT_INC
#	define ENTROPY_VULKAN_DESCRIPTORSETLAYOUT_INC

#	include "Device.hh"

	namespace Entropy
	{
		namespace Vulkan
		{
			class DescriptorSetLayout
			{
				public:
					class Binding
					{
						public:
							Binding(
								const std::uint32_t,
								const VkShaderStageFlags,
								const std::uint32_t = 1,
								const VkDescriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER
							);
							const VkDescriptorSetLayoutBinding &Value() const;
						private:
							VkDescriptorSetLayoutBinding _value;
					};
				public:
					DescriptorSetLayout(const std::shared_ptr<Device> &, const std::vector<Binding> &);
					~DescriptorSetLayout();
					const VkDescriptorSetLayout &Handle() const;
				private:
					std::shared_ptr<Device> _device;
					VkDescriptorSetLayout _handle;
			};
		}
	}

#endif
