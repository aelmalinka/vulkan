/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "RenderPass.hh"

using namespace Entropy::Vulkan;
using namespace std;

RenderPass::RenderPass(
	const shared_ptr<Device> &device,
	const vector<Attachment> &attachments,
	const vector<Subpass> &subpasses
) :
	_device(device),
	_handle(VK_NULL_HANDLE)
{
	vector<VkAttachmentDescription> att;
	vector<VkSubpassDescription> sub;

	att.reserve(attachments.size());
	sub.reserve(subpasses.size());

	for(auto &i : attachments) {
		att.emplace_back(i.Value());
	}
	for(auto &i : subpasses) {
		sub.emplace_back(i.Value());
	}

	VkRenderPassCreateInfo create = {};
	create.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	create.attachmentCount = static_cast<uint32_t>(att.size());
	create.pAttachments = att.data();
	create.subpassCount = static_cast<uint32_t>(sub.size());
	create.pSubpasses = sub.data();

	VkSubpassDependency dep = {};
	dep.srcSubpass = VK_SUBPASS_EXTERNAL;
	dep.dstSubpass = 0;
	dep.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dep.srcAccessMask = 0;
	dep.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dep.dstAccessMask =
		VK_ACCESS_COLOR_ATTACHMENT_READ_BIT |
		VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT
	;

	create.dependencyCount = 1;
	create.pDependencies = &dep;

	EVK_SUCCESS_OR_THROW(
		vkCreateRenderPass(_device->Handle(), &create, nullptr, &_handle),
		"Failed to create Render Pass"
	);
}

RenderPass::~RenderPass()
{
	vkDestroyRenderPass(_device->Handle(), _handle, nullptr);
}

const VkRenderPass &RenderPass::Handle() const
{
	return _handle;
}

RenderPass::Attachment::Attachment(
	const VkFormat format,
	const VkAttachmentLoadOp load,
	const VkImageLayout fin,
	const VkSampleCountFlagBits samples,
	const VkAttachmentStoreOp store,
	const VkImageLayout init,
	const VkAttachmentLoadOp stencload,
	const VkAttachmentStoreOp stencstore
) :
	_value{
		{},
		format,
		samples,
		load,
		store,
		stencload,
		stencstore,
		init,
		fin
	}
{}

const VkAttachmentDescription &RenderPass::Attachment::Value() const
{
	return _value;
}

RenderPass::Subpass::Subpass(
	const VkPipelineBindPoint bind,
	const vector<VkAttachmentReference> &input,
	const vector<VkAttachmentReference> &color
) :
	_input(input),
	_color(color),
	_value()
{
	_value.pipelineBindPoint = bind;
	_value.inputAttachmentCount = static_cast<uint32_t>(_input.size());
	_value.colorAttachmentCount = static_cast<uint32_t>(_color.size());
	_value.pInputAttachments = _input.data();
	_value.pColorAttachments = _color.data();
}

const VkSubpassDescription &RenderPass::Subpass::Value() const
{
	return _value;
}