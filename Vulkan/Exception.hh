/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_VULKAN_EXCEPTION_INC
#	define ENTROPY_VULKAN_EXCEPTION_INC

#	include <cstdint>
#	include <Entropy/Exception.hh>
#	include <Entropy/Log.hh>
#	include <vulkan/vulkan.h>

	namespace Entropy
	{
		namespace Vulkan
		{
			ENTROPY_EXCEPTION_BASE(Exception, "Vulkan Exception");
			ENTROPY_ERROR_INFO(Result, VkResult);

			using Entropy::Severity;
			extern Log::Logger Log;
		}
	}

#	define EVK_SUCCESS_OR_THROW(f, x) { \
		VkResult ok = (f); \
		if(ok != VK_SUCCESS) \
			::boost::throw_exception(::boost::enable_error_info(::Entropy::Vulkan::Exception(x)) << \
			::boost::throw_function(BOOST_CURRENT_FUNCTION) << \
			::boost::throw_file(__FILE__) << \
			::boost::throw_line((int)__LINE__) << \
			::Entropy::Vulkan::Result(ok)); \
	}

#endif
