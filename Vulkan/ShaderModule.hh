/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_VULKAN_SHADERMODULE_INC
#	define ENTROPY_VULKAN_SHADERMODULE_INC

#	include "Device.hh"

	namespace Entropy
	{
		namespace Vulkan
		{
			class ShaderModule
			{
				public:
					ShaderModule(const std::shared_ptr<Device> &, const std::string &);
					~ShaderModule();
					const std::shared_ptr<Device> &getDevice() const;
					const VkShaderModule &Handle() const;
					const std::string &Name() const;
				private:
					static std::vector<char> _get_code(const std::string &);
				private:
					std::shared_ptr<Device> _device;
					VkShaderModule _handle;
					std::string _name;
					std::vector<char> _code;
			};
		}
	}

#endif
