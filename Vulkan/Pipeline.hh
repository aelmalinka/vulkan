/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_VULKAN_PIPELINE_INC
#	define ENTROPY_VULKAN_PIPELINE_INC

#	include "Shader.hh"
#	include "RenderPass.hh"
#	include "DescriptorSetLayout.hh"

	namespace Entropy
	{
		namespace Vulkan
		{
			class Pipeline
			{
				public:
					class VertexInput;
					class InputAssembly;
					class Viewport;
					class Rasterization;
					class Multisample;
					class ColorBlend;
					class Layout;
				public:
					// 2018-09-08 AMR TODO: Add default constructor (as few params as reasonable)
					Pipeline(
						const std::shared_ptr<Device> &,
						const Shader &,
						const std::shared_ptr<RenderPass> &,
						const VertexInput &,
						const InputAssembly &,
						const Viewport &,
						const Rasterization &,
						const Multisample &,
						const ColorBlend &,
						const std::vector<std::shared_ptr<DescriptorSetLayout>> &
					);
					~Pipeline();
					const std::shared_ptr<Device> &getDevice() const;
					const VkPipeline &Handle() const;
				public:
					// 2018-09-08 AMR TODO: Allow setting values
					class VertexInput
					{
						public:
							// 2018-09-17 AMR TODO: use wrapper types?
							VertexInput(
								const std::vector<VkVertexInputBindingDescription> & = {},
								const std::vector<VkVertexInputAttributeDescription> & = {}
							);
							const VkPipelineVertexInputStateCreateInfo &Info() const;
						private:
							VkPipelineVertexInputStateCreateInfo _info;
					};
					class InputAssembly
					{
						public:
							InputAssembly();
							const VkPipelineInputAssemblyStateCreateInfo &Info() const;
						private:
							VkPipelineInputAssemblyStateCreateInfo _info;
					};
					class Rasterization
					{
						public:
							Rasterization();
							const VkPipelineRasterizationStateCreateInfo &Info() const;
						private:
							VkPipelineRasterizationStateCreateInfo _info;
					};
					// 2018-09-08 AMR TODO: allow specifying multiple viewports or scissors
					class Viewport
					{
						public:
							explicit Viewport(
								const VkExtent2D &,
								const VkOffset2D & = {},
								const float = 0.0f,
								const float = 1.0f
							);
							Viewport(
								const VkExtent2D &,
								const VkExtent2D &,
								const VkOffset2D & = {},
								const VkOffset2D & = {},
								const float = 0.0f,
								const float = 1.0f
							);
							const VkPipelineViewportStateCreateInfo &Info() const;
						private:
							VkPipelineViewportStateCreateInfo _info;
							VkViewport _view;
							VkRect2D _scissor;
					};
					class Multisample
					{
						public:
							Multisample();
							const VkPipelineMultisampleStateCreateInfo &Info() const;
						private:
							VkPipelineMultisampleStateCreateInfo _info;
					};
					// 2018-09-08 AMR TODO: allow multiple blend attachments
					class ColorBlend
					{
						public:
							ColorBlend();
							const VkPipelineColorBlendStateCreateInfo &Info() const;
						private:
							VkPipelineColorBlendStateCreateInfo _info;
							VkPipelineColorBlendAttachmentState _blend;
					};
					class Layout
					{
						public:
							explicit Layout(
								const std::shared_ptr<Device> &,
								const std::vector<std::shared_ptr<DescriptorSetLayout>> &
							);
							~Layout();
							const VkPipelineLayout &Handle() const;
						private:
							std::shared_ptr<Device> _device;
							std::vector<std::shared_ptr<DescriptorSetLayout>> _layouts;
							VkPipelineLayout _handle;
					};
				private:
					std::shared_ptr<Device> _device;
					std::shared_ptr<RenderPass> _pass;
					Layout _layout;
					VkPipeline _handle;
			};
		}
	}

#endif
