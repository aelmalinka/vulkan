/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Pipeline.hh"

using namespace Entropy::Vulkan;
using namespace std;

Pipeline::InputAssembly::InputAssembly() :
	_info()
{
	_info.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	_info.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	_info.primitiveRestartEnable = VK_FALSE;
}

const VkPipelineInputAssemblyStateCreateInfo &Pipeline::InputAssembly::Info() const
{
	return _info;
}
