/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Event.hh"

using namespace Entropy::Vulkan;
using namespace std;

Event::Event(const size_t &id) :
	Entropy::Event(id)
{}

Event::~Event() = default;
