/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_VULKAN_SEMAPHORE_INC
#	define ENTROPY_VULKAN_SEMAPHORE_INC

#	include "Device.hh"

	namespace Entropy
	{
		namespace Vulkan
		{
			class Semaphore
			{
				public:
					explicit Semaphore(const std::shared_ptr<Device> &);
					~Semaphore();
					const VkSemaphore &Handle() const;
				private:
					std::shared_ptr<Device> _device;
					VkSemaphore _handle;
			};
		}
	}

#endif
