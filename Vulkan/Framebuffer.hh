/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_VULKAN_FRAMEBUFFER_INC
#	define ENTROPY_VULKAN_FRAMEBUFFER_INC

#	include "SwapChain.hh"
#	include "RenderPass.hh"
#	include "Queue.hh"

	namespace Entropy
	{
		namespace Vulkan
		{
			// 2018-09-10 AMR TODO: Framebuffer w/o ImageView?
			// 2018-09-10 AMR TODO: Framebuffer w/o RenderPass?
			// 2018-09-10 AMR TODO: Framebuffer w/o SwapChain?
			class Framebuffer
			{
				public:
					Framebuffer(const std::shared_ptr<ImageView> &, const std::shared_ptr<RenderPass> &, const std::shared_ptr<SwapChain> &, const std::uint32_t);
					~Framebuffer();
					const std::shared_ptr<RenderPass> &getRenderPass() const;
					const VkFramebuffer &Handle() const;
					const VkExtent2D &Extent() const;
					void Present(const std::shared_ptr<Queue> &);
					static std::vector<std::shared_ptr<Framebuffer>> Get(const std::shared_ptr<SwapChain> &, const std::shared_ptr<RenderPass> &);
				private:
					std::shared_ptr<ImageView> _img;
					std::shared_ptr<RenderPass> _render;
					std::shared_ptr<SwapChain> _swap;
					VkFramebuffer _handle;
					std::uint32_t _idx;
					VkExtent2D _extent;
			};
		}
	}

#endif
