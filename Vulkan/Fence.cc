/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Fence.hh"

using namespace Entropy::Vulkan;
using namespace std;

Fence::Fence(const shared_ptr<Device> &device) :
	_device(device),
	_handle(VK_NULL_HANDLE)
{
	VkFenceCreateInfo create = {};
	create.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	create.flags = VK_FENCE_CREATE_SIGNALED_BIT;

	EVK_SUCCESS_OR_THROW(
		vkCreateFence(_device->Handle(), &create, nullptr, &_handle),
		"Failed to create Fence"
	);
}

Fence::~Fence()
{
	vkDestroyFence(_device->Handle(), _handle, nullptr);
}

const shared_ptr<Device> &Fence::getDevice() const
{
	return _device;
}

const VkFence &Fence::Handle() const
{
	return _handle;
}

void Fence::Wait(const uint64_t amt)
{
	vector<shared_ptr<Fence>> v = {
		shared_from_this()
	};

	Wait(v, true, amt);
}

void Fence::Reset()
{
	vector<shared_ptr<Fence>> v = {
		shared_from_this()
	};

	Reset(v);
}

void Fence::Wait(const vector<shared_ptr<Fence>> &fences, const bool all, const uint64_t amt)
{
	vector<VkFence> f;
	shared_ptr<Device> d;
	f.reserve(fences.size());

	for(auto &fence : fences) {
		if(d) {
			if(d != fence->getDevice())
				ENTROPY_THROW(Exception("Locking multiple Fences requires them to be on the same Device"));
		} else {
			d = fence->getDevice();
		}

		f.emplace_back(fence->Handle());
	}

	EVK_SUCCESS_OR_THROW(
		vkWaitForFences(d->Handle(), static_cast<uint32_t>(fences.size()), f.data(), (all) ? VK_TRUE : VK_FALSE, amt),
		"Failed to wait for fence"
	);
}

void Fence::Reset(const vector<shared_ptr<Fence>> &fences)
{
	vector<VkFence> f;
	shared_ptr<Device> d;
	f.reserve(fences.size());

	for(auto &fence : fences) {
		if(d) {
			if(d != fence->getDevice())
				ENTROPY_THROW(Exception("Resetting multiple Fences requires them to be on the same Device"));
		} else {
			d = fence->getDevice();
		}

		f.emplace_back(fence->Handle());
	}

	EVK_SUCCESS_OR_THROW(
		vkResetFences(d->Handle(), static_cast<uint32_t>(fences.size()), f.data()),
		"Failed to reset fence"
	);
}
