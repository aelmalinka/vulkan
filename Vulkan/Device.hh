/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_VULKAN_DEVICE_INC
#	define ENTROPY_VULKAN_DEVICE_INC

#	include "PhysicalDevice.hh"

	namespace Entropy
	{
		namespace Vulkan
		{
			class Queue;

			class Device :
				public std::enable_shared_from_this<Device>
			{
				public:
					Device(const std::shared_ptr<PhysicalDevice> &, const std::vector<QueueFamily> &, const std::vector<const char *> &);
					~Device();
					const std::shared_ptr<PhysicalDevice> &getPhysical() const;
					const VkDevice &Handle() const;
					std::vector<std::shared_ptr<Queue>> &Queues();
					void Wait() const;
				private:
					std::shared_ptr<PhysicalDevice> _physical;
					VkDevice _handle;
					std::vector<std::shared_ptr<Queue>> _queues;
			};
		}
	}

#	include "Queue.hh"

#endif
