/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Queue.hh"

using namespace Entropy::Vulkan;
using namespace std;

Queue::Queue(const VkQueue handle, const QueueFamily &family, const uint32_t i) :
	_handle(handle),
	_family(family),
	_index(i)
{}

const VkQueue &Queue::Handle() const
{
	return _handle;
}

const uint32_t &Queue::Index() const
{
	return _index;
}

const QueueFamily &Queue::Family() const
{
	return _family;
}
