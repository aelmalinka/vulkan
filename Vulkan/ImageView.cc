/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "ImageView.hh"

using namespace Entropy::Vulkan;
using namespace std;

ImageView::ImageView(const shared_ptr<Device> &device, const VkImage image, const VkFormat format) :
	_device(device),
	_handle(VK_NULL_HANDLE),
	_format(format)
{
	VkImageViewCreateInfo create = {};
	create.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	create.image = image;
	create.viewType = VK_IMAGE_VIEW_TYPE_2D;
	create.format = format;

	// 2018-09-07 AMR TODO: allow setting swizzle
	create.components.r = VK_COMPONENT_SWIZZLE_R;
	create.components.g = VK_COMPONENT_SWIZZLE_G;
	create.components.b = VK_COMPONENT_SWIZZLE_B;
	create.components.a = VK_COMPONENT_SWIZZLE_A;

	// 2018-09-07 AMR TODO: allow setting
	create.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	create.subresourceRange.baseMipLevel = 0;
	create.subresourceRange.levelCount = 1;
	create.subresourceRange.baseArrayLayer = 0;
	create.subresourceRange.layerCount = 1;

	EVK_SUCCESS_OR_THROW(
		vkCreateImageView(device->Handle(), &create, nullptr, &_handle),
		"Failed to create ImageView"
	);
}

ImageView::~ImageView()
{
	vkDestroyImageView(_device->Handle(), _handle, nullptr);
}

const shared_ptr<Device> &ImageView::getDevice() const
{
	return _device;
}

const VkImageView &ImageView::Handle() const
{
	return _handle;
}

const VkFormat &ImageView::Format() const
{
	return _format;
}
