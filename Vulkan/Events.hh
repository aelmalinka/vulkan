/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_VULKAN_EVENTS_INC
#	define ENTROPY_VULKAN_EVENTS_INC

#	include "Event.hh"

	namespace Entropy
	{
		namespace Vulkan
		{
			namespace Events
			{
				class Debug :
					public Event
				{
					public:
						static constexpr std::size_t Id = Event::First;
					public:
						Debug(
							const VkDebugUtilsMessageSeverityFlagBitsEXT,
							const VkDebugUtilsMessageTypeFlagsEXT,
							const VkDebugUtilsMessengerCallbackDataEXT *
						);
						const VkDebugUtilsMessageSeverityFlagBitsEXT &Severity() const;
						const VkDebugUtilsMessageTypeFlagsEXT &Type() const;
						const VkDebugUtilsMessengerCallbackDataEXT *Data() const;
						Log::Severity LogSeverity() const;
						std::string TypeName() const;
						std::string Message() const;
					private:
						VkDebugUtilsMessageSeverityFlagBitsEXT _severity;
						VkDebugUtilsMessageTypeFlagsEXT _type;
						const VkDebugUtilsMessengerCallbackDataEXT *_data;
				};
			}
		}
	}

#endif
