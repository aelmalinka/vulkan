/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "ApplicationInfo.hh"

using namespace Entropy::Vulkan;
using namespace std;

ApplicationInfo::ApplicationInfo(
	const char *name,
	const uint32_t version,
	vector<const char *> &&extensions,
	vector<const char *> &&optext,
	vector<const char *> &&layers,
	vector<const char *> &&optlay
) :
	_handle(),
	_extensions(extensions),
	_layers(layers)
{
	_handle.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	_handle.pApplicationName = name;
	_handle.applicationVersion = version;
	_handle.pEngineName = "Entropy Vulkan";
	_handle.engineVersion = Version;
	_handle.apiVersion = VK_API_VERSION_1_0;

	uint32_t extcnt = 0;
	EVK_SUCCESS_OR_THROW(vkEnumerateInstanceExtensionProperties(nullptr, &extcnt, nullptr), "Failed to get Extension count");
	
	uint32_t laycnt = 0;
	EVK_SUCCESS_OR_THROW(vkEnumerateInstanceLayerProperties(&laycnt, nullptr), "Failed to get Layer count");

	vector<VkExtensionProperties> ext(extcnt);
	vector<VkLayerProperties> lay(laycnt);

	EVK_SUCCESS_OR_THROW(vkEnumerateInstanceExtensionProperties(nullptr, &extcnt, ext.data()), "Failed to get Extensions");
	EVK_SUCCESS_OR_THROW(vkEnumerateInstanceLayerProperties(&laycnt, lay.data()), "Failed to get Layers");

	for(const auto &i : ext)
		ENTROPY_LOG(Log, Severity::Debug) << i.extensionName << " Extension found";

	for(const auto &i : lay)
		ENTROPY_LOG(Log, Severity::Debug) << i.layerName << " Layer found";

	for(const auto &i : optext)
		for(const auto &e : ext)
			if(strcmp(i, e.extensionName) == 0)
				_extensions.push_back(i);

	for(const auto &i : optlay)
		for(const auto &l : lay)
			if(strcmp(i, l.layerName) == 0)
				_layers.push_back(i);

	for(const auto &i : _extensions)
		ENTROPY_LOG(Log, Severity::Info) << "Loading Extension " << i;
	for(const auto &i : _layers)
		ENTROPY_LOG(Log, Severity::Info) << "Loading Layer " << i;
}

ApplicationInfo::ApplicationInfo(const ApplicationInfo &) = default;
ApplicationInfo::ApplicationInfo(ApplicationInfo &&) = default;
ApplicationInfo::~ApplicationInfo() = default;

VkApplicationInfo &ApplicationInfo::Handle()
{
	return _handle;
}

const vector<const char *> &ApplicationInfo::Extensions() const
{
	return _extensions;
}

const vector<const char *> &ApplicationInfo::Layers() const
{
	return _layers;
}
