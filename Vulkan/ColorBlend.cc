/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Pipeline.hh"

using namespace Entropy::Vulkan;
using namespace std;

Pipeline::ColorBlend::ColorBlend() :
	_info(),
	_blend()
{
	_blend.blendEnable = VK_TRUE;
	_blend.colorWriteMask =
		VK_COLOR_COMPONENT_R_BIT |
		VK_COLOR_COMPONENT_G_BIT |
		VK_COLOR_COMPONENT_B_BIT |
		VK_COLOR_COMPONENT_A_BIT
	;
	_blend.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
	_blend.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
	_blend.colorBlendOp = VK_BLEND_OP_ADD;
	_blend.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
	_blend.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
	_blend.alphaBlendOp = VK_BLEND_OP_ADD;

	_info.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	_info.logicOpEnable = VK_FALSE;
	_info.attachmentCount = 1;
	_info.pAttachments = &_blend;
}

const VkPipelineColorBlendStateCreateInfo &Pipeline::ColorBlend::Info() const
{
	return _info;
}
