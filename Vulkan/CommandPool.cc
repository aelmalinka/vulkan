/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "CommandPool.hh"

using namespace Entropy::Vulkan;
using namespace std;

CommandPool::CommandPool(const shared_ptr<Device> &device, const shared_ptr<Queue> &queue) :
	_device(device),
	_queue(queue),
	_handle(VK_NULL_HANDLE),
	_allocs()
{
	VkCommandPoolCreateInfo create = {};

	create.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	create.queueFamilyIndex = _queue->Index();

	EVK_SUCCESS_OR_THROW(
		vkCreateCommandPool(_device->Handle(), &create, nullptr, &_handle),
		"Failed to create command pool"
	);
}

CommandPool::~CommandPool()
{
	vkDestroyCommandPool(_device->Handle(), _handle, nullptr);
}

const shared_ptr<Device> &CommandPool::getDevice() const
{
	return _device;
}

const shared_ptr<Queue> &CommandPool::getQueue() const
{
	return _queue;
}

const VkCommandPool &CommandPool::Handle() const
{
	return _handle;
}
