/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_VULKAN_SHADER_INC
#	define ENTROPY_VULKAN_SHADER_INC

#	include "ShaderModule.hh"

	namespace Entropy
	{
		namespace Vulkan
		{
			// 2018-09-08 AMR TODO: Tesselation
			class Shader
			{
				public:
					explicit Shader(
						const std::string &,
						const std::shared_ptr<ShaderModule> & = std::shared_ptr<ShaderModule>(),
						const std::shared_ptr<ShaderModule> & = std::shared_ptr<ShaderModule>(),
						const std::shared_ptr<ShaderModule> & = std::shared_ptr<ShaderModule>()
					);
					const std::string &name() const;
					const std::size_t size() const;
					const VkPipelineShaderStageCreateInfo *data() const;
				private:
					std::string _name;
					std::shared_ptr<ShaderModule> _vertex;
					std::shared_ptr<ShaderModule> _fragment;
					std::shared_ptr<ShaderModule> _geometry;
					std::vector<VkPipelineShaderStageCreateInfo> _info;
			};
		}
	}

#endif
