/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Events.hh"

using namespace Entropy::Vulkan::Events;
using namespace std;

Debug::Debug(
	const VkDebugUtilsMessageSeverityFlagBitsEXT sev,
	const VkDebugUtilsMessageTypeFlagsEXT type,
	const VkDebugUtilsMessengerCallbackDataEXT *data
) :
	Event(Id),
	_severity(sev),
	_type(type),
	_data(data)
{}

const VkDebugUtilsMessageSeverityFlagBitsEXT &Debug::Severity() const
{
	return _severity;
}

const VkDebugUtilsMessageTypeFlagsEXT &Debug::Type() const
{
	return _type;
}

const VkDebugUtilsMessengerCallbackDataEXT *Debug::Data() const
{
	return _data;
}

Entropy::Log::Severity Debug::LogSeverity() const
{
	if(Severity() >= VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT)
		return Severity::Error;
	else if(Severity() >= VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT)
		return Severity::Warning;
	else if(Severity() >= VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT)
		return Severity::Info;
	else if(Severity() >= VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT)
		return Severity::Debug;
	else
		return Severity::Error;
}

string Debug::TypeName() const
{
	switch(Type())
	{
		case VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT:
			return "General"s;
		case VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT:
			return "Validation"s;
		case VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT:
			return "Performance"s;
		default:
			return "Unknown"s;
	}
}

string Debug::Message() const
{
	return TypeName() + ": "s + Data()->pMessage;
}
