/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_VULKAN_PHYSICAL_DEVICE_INC
#	define ENTROPY_VULKAN_PHYSICAL_DEVICE_INC

#	include "QueueFamily.hh"
#	include <functional>
#	include <memory>
#	include <set>

	namespace Entropy
	{
		namespace Vulkan
		{
			// 2018-08-29 AMR TODO: multiple graphics cards
			// 2018-09-04 AMR TODO: multiple surfaces
			class PhysicalDevice :
				public std::enable_shared_from_this<PhysicalDevice>
			{
				public:
					PhysicalDevice(const std::shared_ptr<Instance> &, VkPhysicalDevice);
					PhysicalDevice(const PhysicalDevice &) = delete;
					const VkPhysicalDevice &Handle() const;
					const std::shared_ptr<Instance> &getInstance() const;
					// 2018-09-17 AMR TODO: cache these or get on fly?
					const VkPhysicalDeviceProperties &Properties() const;
					const VkPhysicalDeviceFeatures &Features() const;
					const VkPhysicalDeviceMemoryProperties &Memory() const;
					const std::vector<QueueFamily> &Queues();
					const Surface::Capabilities &Capabilites(const std::shared_ptr<Surface> &);
					const std::vector<Surface::Format> &Formats(const std::shared_ptr<Surface> &);
					const std::vector<Surface::PresentMode> &Modes(const std::shared_ptr<Surface> &);
					bool hasExtension(const std::string &) const;
					// 2018-09-04 AMR TODO/NOTE/XXX: do we really want PhysicalDevice modifiable in this callback?
					static std::shared_ptr<PhysicalDevice> Get(const std::shared_ptr<Instance> &, const std::function<int(const std::shared_ptr<PhysicalDevice> &)> &);
				private:
					std::shared_ptr<Instance> _instance;
					VkPhysicalDevice _handle;
					VkPhysicalDeviceProperties _properties;
					VkPhysicalDeviceFeatures _features;
					VkPhysicalDeviceMemoryProperties _memory;
					std::vector<QueueFamily> _queues;
					std::shared_ptr<Surface::Capabilities> _caps;
					std::vector<Surface::Format> _formats;
					std::vector<Surface::PresentMode> _modes;
					std::set<std::string> _extensions;
			};
		}
	}

#endif
