/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Semaphore.hh"

using namespace Entropy::Vulkan;
using namespace std;

Semaphore::Semaphore(const shared_ptr<Device> &device) :
	_device(device),
	_handle(VK_NULL_HANDLE)
{
	VkSemaphoreCreateInfo create = {};

	create.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

	EVK_SUCCESS_OR_THROW(
		vkCreateSemaphore(_device->Handle(), &create, nullptr, &_handle),
		"Failed to create semaphore"
	);
}

Semaphore::~Semaphore()
{
	vkDestroySemaphore(_device->Handle(), _handle, nullptr);
}

const VkSemaphore &Semaphore::Handle() const
{
	return _handle;
}
