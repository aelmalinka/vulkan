/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_VULKAN_BUFFER_IMPL
#	define ENTROPY_VULKAN_BUFFER_IMPL

#	include "Buffer.hh"
#	include <type_traits>

	namespace Entropy
	{
		namespace Vulkan
		{
			namespace detail
			{
				template<typename T, typename F, typename = void>
				struct has_size : std::false_type {};
				template<typename T, typename F, typename = void>
				struct has_data : std::false_type {};

				template<typename T, typename R, typename ...A>
				struct has_size<
					T,
					R(A...),
					typename std::enable_if<
						std::is_same<R, void>::value ||
						std::is_convertible<decltype(
							std::declval<T>().size(std::declval<A>()...)
						), R>::value
					>::type
				> : std::true_type {};

				template<typename T, typename R, typename ...A>
				struct has_data<
					T,
					R(A...),
					typename std::enable_if<
						std::is_same<R, void>::value ||
						std::is_convertible<decltype(
							std::declval<T>().data(std::declval<A>()...)
						), R>::value
					>::type
				> : std::true_type {};

				template<typename T, typename = void>
				struct get_size
				{
					static std::size_t apply(const T &)
					{
						return sizeof(T);
					}
				};

				template<typename T>
				struct get_size<
					T,
					typename std::enable_if<
						has_size<T, std::size_t() const>::value
					>::type
				> {
					static std::size_t apply(const T &t)
					{
						return t.size();
					}
				};

				template<typename T>
				struct get_size<
					T,
					typename std::enable_if<
						std::is_pointer<T>::value
					>::type
				> {
					static std::size_t apply(const T *t)
					{
						if(t)
							return sizeof(*t);
						else
							return 0;
					}
				};

				template<typename T, typename = void>
				struct get_data
				{
					static const T *apply(const T &t)
					{
						return &t;
					}
				};

				template<typename T>
				struct get_data<
					T,
					typename std::enable_if<
						has_data<T, const typename T::value_type *() const>::value
					>::type
				> {
					static const auto *apply(const T &t)
					{
						return t.data();
					}
				};

				template<typename T>
				struct get_data<
					T,
					typename std::enable_if<
						std::is_pointer<T>::value
					>::type
				> {
					static const T *apply(const T *t)
					{
						return t;
					}
				};
			}

			template<typename T>
			void Buffer::Write(const T &t) const
			{
				// 2018-11-02 AMR TODO: rigorous testing
				auto sz = detail::get_size<T>::apply(t);
				const auto *d = detail::get_data<T>::apply(t);

				if(sz > _sz)
					ENTROPY_THROW(Exception("Vertex array is too large for Buffer"));

				void *data;
				EVK_SUCCESS_OR_THROW(
					vkMapMemory(_device->Handle(), _memory, 0, sz, 0, &data),
					"Failed to map buffer memory"
				);
				memcpy(data, d, sz);
				vkUnmapMemory(_device->Handle(), _memory);
			}
		}
	}

#endif
