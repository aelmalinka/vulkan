/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Framebuffer.hh"

using namespace Entropy::Vulkan;
using namespace std;

Framebuffer::Framebuffer(
	const shared_ptr<ImageView> &img,
	const shared_ptr<RenderPass> &render,
	const shared_ptr<SwapChain> &swap,
	const std::uint32_t idx
) :
	_img(img),
	_render(render),
	_swap(swap),
	_handle(VK_NULL_HANDLE),
	_idx(idx),
	_extent(swap->Extent())
{
	VkFramebufferCreateInfo create = {};

	create.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
	create.renderPass = _render->Handle();
	create.attachmentCount = 1;
	create.pAttachments = &_img->Handle();
	create.width = _extent.width;
	create.height = _extent.height;
	create.layers = 1;

	EVK_SUCCESS_OR_THROW(
		vkCreateFramebuffer(_img->getDevice()->Handle(), &create, nullptr, &_handle),
		"Failed to create framebuffer"
	);
}

Framebuffer::~Framebuffer()
{
	vkDestroyFramebuffer(_img->getDevice()->Handle(), _handle, nullptr);
}

const shared_ptr<RenderPass> &Framebuffer::getRenderPass() const
{
	return _render;
}

const VkFramebuffer &Framebuffer::Handle() const
{
	return _handle;
}

const VkExtent2D &Framebuffer::Extent() const
{
	return _extent;
}

void Framebuffer::Present(const shared_ptr<Queue> &present)
{
	VkPresentInfoKHR info = {};
	info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	info.swapchainCount = 1;
	info.pSwapchains = &_swap->Handle();
	info.pImageIndices = &_idx;

	EVK_SUCCESS_OR_THROW(
		vkQueuePresentKHR(present->Handle(), &info),
		"Failed to present"
	);
}

vector<shared_ptr<Framebuffer>> Framebuffer::Get(const shared_ptr<SwapChain> &swap, const shared_ptr<RenderPass> &render)
{
	vector<shared_ptr<Framebuffer>> ret;
	auto x = 0u;

	ret.reserve(swap->size());
	for(auto &i : *swap) {
		ret.emplace_back(make_shared<Framebuffer>(i, render, swap, x++));
	}

	return ret;
}
