/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_VULKAN_BUFFER_INC
#	define ENTROPY_VULKAN_BUFFER_INC

#	include "Device.hh"

	namespace Entropy
	{
		namespace Vulkan
		{
			class Buffer
			{
				public:
					// 2018-09-17 AMR TODO: Usage
					// 2018-09-17 AMR TODO: Buffer w/o Memory?
					// 2018-09-17 AMR TODO: rebindable memory?
					// 2018-09-17 AMR TODO: look into vk{Flush,Invalidate}MappedMemoryRanges
					Buffer(
						const std::shared_ptr<Device> &,
						const VkDeviceSize,
						const VkBufferUsageFlags,
						const std::function<std::uint32_t(const std::uint32_t, const VkMemoryPropertyFlags)> &);
					~Buffer();
					const VkBuffer &Handle() const;
					template<typename T>
					void Write(const T &) const;
				private:
					std::shared_ptr<Device> _device;
					VkBuffer _handle;
					VkDeviceMemory _memory;
					VkDeviceSize _sz;
			};
		}
	}

#	include "Buffer.impl.hh"

#endif
