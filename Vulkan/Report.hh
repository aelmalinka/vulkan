/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_VULKAN_REPORT_INC
#	define ENTROPY_VULKAN_REPORT_INC

#	include "Instance.hh"
#	include "Events.hh"
#	include <functional>
#	include <Entropy/SharedData.hh>

	namespace Entropy
	{
		namespace Vulkan
		{
			namespace detail
			{
				class calls
				{
					public:
						explicit calls(const std::shared_ptr<Instance> &);
						VkResult Create(VkInstance, const VkDebugUtilsMessengerCreateInfoEXT *, const VkAllocationCallbacks *, VkDebugUtilsMessengerEXT *) const;
						void Destroy(VkInstance, VkDebugUtilsMessengerEXT, const VkAllocationCallbacks *) const;
					private:
						PFN_vkCreateDebugUtilsMessengerEXT _create;
						PFN_vkDestroyDebugUtilsMessengerEXT _destroy;
				};
			}

			class Report :
				private SharedData<detail::calls>
			{
				public:
					Report(const std::shared_ptr<Instance> &, const std::function<void(const Events::Debug &)> &);
					~Report();
				private:
					const std::shared_ptr<Instance> _instance;
					VkDebugUtilsMessengerEXT _handle;
					std::function<void(const Events::Debug &)> _cb;
			};
		}
	}

#endif
