/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "SwapChain.hh"
#include <limits>
#include <cmath>

using namespace Entropy::Vulkan;
using namespace std;

SwapChain::SwapChain(const shared_ptr<Device> &device, const shared_ptr<Surface> &surface) :
	_device(device),
	_surface(surface),
	_format(getFormats(device->getPhysical()->Formats(_surface))),
	_present(getPresent(device->getPhysical()->Modes(_surface))),
	_extent(getExtent(device->getPhysical()->Capabilites(_surface))),
	_handle(VK_NULL_HANDLE),
	_images()
{
	VkSwapchainCreateInfoKHR create = {};
	create.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	create.surface = surface->Handle();

	uint32_t count = getCount(device->getPhysical()->Capabilites(_surface));
	vector<uint32_t> a;

	create.minImageCount = count;
	create.imageFormat = _format.format;
	create.imageColorSpace = _format.colorSpace;
	create.imageExtent = _extent;
	create.imageArrayLayers = 1;
	create.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	create.presentMode = _present;

	// 2018-09-06 AMR TODO: this requires that device is created with queues { graphics, present } or { graphcis_and_present }, FIXME
	// 2018-09-06 AMR TODO: this costs performance when the queues are seperate, FIXME (see ownership)
	if(device->Queues().size() == 1)
		create.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
	else {
		a = {
			device->Queues()[0]->Index(),
			device->Queues()[1]->Index(),
		};
		create.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
		create.queueFamilyIndexCount = 2;
		create.pQueueFamilyIndices = a.data();
	}

	create.preTransform = device->getPhysical()->Capabilites(_surface).CurrentTransform();
	create.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	// 2018-09-06 AMR TODO: to clipping or not? (hidden pixels not posted)
	create.clipped = VK_TRUE;
	create.oldSwapchain = VK_NULL_HANDLE;

	EVK_SUCCESS_OR_THROW(
		vkCreateSwapchainKHR(device->Handle(), &create, nullptr, &_handle),
		"Failed to create swapchain"
	);

	EVK_SUCCESS_OR_THROW(
		vkGetSwapchainImagesKHR(device->Handle(), _handle, &count, nullptr),
		"Failed to get swap chain image count"
	);
	vector<VkImage> images(count);
	_images.reserve(count);
	EVK_SUCCESS_OR_THROW(
		vkGetSwapchainImagesKHR(device->Handle(), _handle, &count, images.data()),
		"Failed to get swap chain images"
	);

	for(const auto &i : images) {
		_images.emplace_back(make_shared<ImageView>(_device, i, _format.format));
	}
}

SwapChain::~SwapChain()
{
	vkDestroySwapchainKHR(_device->Handle(), _handle, nullptr);
}

const shared_ptr<Device> &SwapChain::getDevice() const
{
	return _device;
}

const shared_ptr<Surface> &SwapChain::getSurface() const
{
	return _surface;
}

const VkSwapchainKHR &SwapChain::Handle() const
{
	return _handle;
}

const VkExtent2D &SwapChain::Extent() const
{
	return _extent;
}

const VkFormat &SwapChain::Format() const
{
	return _format.format;
}

vector<shared_ptr<ImageView>>::const_iterator SwapChain::begin() const
{
	return _images.begin();
}

vector<shared_ptr<ImageView>>::const_iterator SwapChain::end() const
{
	return _images.end();
}

size_t SwapChain::size() const
{
	return _images.size();
}

uint32_t SwapChain::NextIndex(const Semaphore &sem)
{
	uint32_t idx;
	EVK_SUCCESS_OR_THROW(
		vkAcquireNextImageKHR(
			_device->Handle(),
			_handle,
			numeric_limits<uint64_t>::max(),
			sem.Handle(),
			VK_NULL_HANDLE,
			&idx
		),
		"Failed to get next swapchain image"
	);
	return idx;
}

VkSurfaceFormatKHR SwapChain::getFormats(const vector<Surface::Format> &formats)
{
	if(formats.size() == 1 && formats[0].Pixels() == VK_FORMAT_UNDEFINED)
		return {
			VK_FORMAT_B8G8R8A8_UNORM,
			VK_COLORSPACE_SRGB_NONLINEAR_KHR
		};

	for(const auto &f : formats) {
		if(
			f.Pixels() == VK_FORMAT_B8G8R8A8_UNORM &&
			f.ColorSpace() == VK_COLORSPACE_SRGB_NONLINEAR_KHR
		)
			return f.Value();
	}

	// 2018-09-07 AMR NOTE: default to first given if preferred isn't available
	return formats[0].Value();
}

VkPresentModeKHR SwapChain::getPresent(const vector<Surface::PresentMode> &presents)
{
	VkPresentModeKHR ret = VK_PRESENT_MODE_IMMEDIATE_KHR;

	for(const auto &p : presents) {
		if(p.Value() == VK_PRESENT_MODE_MAILBOX_KHR)
			return p.Value();
		else if(p.Value() == VK_PRESENT_MODE_IMMEDIATE_KHR)
			ret = p.Value();
	}

	return ret;
}

VkExtent2D SwapChain::getExtent(const Surface::Capabilities &caps)
{
	// 2018-09-06 AMR TODO: this shouldn't be hardcoded
	const uint32_t WIDTH = 800;
	const uint32_t HEIGHT = 600;

	if(caps.CurrentExtent().width != numeric_limits<uint32_t>::max())
		return caps.CurrentExtent();
	else
		return {
			max(caps.MinExtent().width, min(caps.MaxExtent().width, WIDTH)),
			max(caps.MinExtent().height, min(caps.MaxExtent().height, HEIGHT)),
		};
}

uint32_t SwapChain::getCount(const Surface::Capabilities &caps)
{
	uint32_t ret = caps.MinImageCount() + 1;
	
	if(caps.MaxImageCount() > 0 && ret > caps.MaxImageCount())
		return caps.MaxImageCount();

	return ret;
}
