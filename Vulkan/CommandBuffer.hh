/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_VULKAN_COMMANDBUFFER_INC
#	define ENTROPY_VULKAN_COMMANDBUFFER_INC

#	include "Exception.hh"
#	include "Framebuffer.hh"
#	include "Pipeline.hh"
#	include "Fence.hh"
#	include "Buffer.hh"

	namespace Entropy
	{
		namespace Vulkan
		{
			class CommandPool;

			class CommandBuffer
			{
				public:
					CommandBuffer(const std::shared_ptr<CommandPool> &, const VkCommandBuffer &);
					const VkCommandBuffer &Handle() const;
					// 2018-09-17 AMR TODO: RAIIify this
					void Begin();
					void End();
					// 2018-09-17 AMR TODO: RAIIify this
					void BeginRender(const std::shared_ptr<Framebuffer> &);
					void EndRender();
					void Bind(const std::shared_ptr<Pipeline> &);
					// 2018-09-17 AMR TODO: bind multiple buffers
					void Bind(const std::shared_ptr<Buffer> &);
					// 2018-09-17 AMR NOTE: will we ever want instances other than 1?
					void Draw(const std::uint32_t, const std::uint32_t, const std::uint32_t, const std::uint32_t);
					void Submit(
						const std::shared_ptr<Fence> & = std::shared_ptr<Fence>(),
						const std::vector<std::shared_ptr<Semaphore>> & = {},
						const std::vector<std::shared_ptr<Semaphore>> & = {}
					);
				private:
					std::shared_ptr<CommandPool> _pool;
					VkCommandBuffer _handle;
			};
		}
	}

#endif
