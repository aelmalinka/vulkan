/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_VULKAN_IMAGE_VIEW_INC
#	define ENTROPY_VULKAN_IMAGE_VIEW_INC

#	include "Device.hh"

	namespace Entropy
	{
		namespace Vulkan
		{
			class ImageView
			{
				public:
					ImageView(const std::shared_ptr<Device> &, const VkImage, const VkFormat);
					~ImageView();
					const std::shared_ptr<Device> &getDevice() const;
					const VkImageView &Handle() const;
					const VkFormat &Format() const;
				private:
					std::shared_ptr<Device> _device;
					VkImageView _handle;
					VkFormat _format;
			};
		}
	}

#endif
