/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_VULKAN_SURFACE_INC
#	define ENTROPY_VULKAN_SURFACE_INC

#	include "Instance.hh"

	namespace Entropy
	{
		namespace Vulkan
		{
			class PhysicalDevice;

			class Surface
			{
				public:
					template<typename Window>
					Surface(const std::shared_ptr<Instance> &, Window);
					~Surface();
					const VkSurfaceKHR &Handle() const;
					const std::shared_ptr<Instance> &getInstance() const;
				private:
					std::shared_ptr<Instance> _instance;
					VkSurfaceKHR _handle;
				public:
					class Capabilities
					{
						public:
							Capabilities(const std::shared_ptr<PhysicalDevice> &, const std::shared_ptr<Surface> &);
							// 2018-09-04 AMR TODO: Do we want this? and how do we want this?
							const std::shared_ptr<Surface> &getSurface() const;
							const VkSurfaceTransformFlagBitsKHR &CurrentTransform() const;
							const uint32_t &MinImageCount() const;
							const uint32_t &MaxImageCount() const;
							const VkExtent2D &CurrentExtent() const;
							const VkExtent2D &MinExtent() const;
							const VkExtent2D &MaxExtent() const;
						private:
							VkSurfaceCapabilitiesKHR _caps;
							std::shared_ptr<Surface> _surface;
					};
					class Format
					{
						public:
							explicit Format(const VkSurfaceFormatKHR &);
							const VkFormat &Pixels() const;
							const VkColorSpaceKHR &ColorSpace() const;
							const VkSurfaceFormatKHR &Value() const;
							static std::vector<Format> Get(const std::shared_ptr<PhysicalDevice> &, const std::shared_ptr<Surface> &);
						private:
							VkSurfaceFormatKHR _format;
					};
					class PresentMode
					{
						public:
							explicit PresentMode(const VkPresentModeKHR &);
							static std::vector<PresentMode> Get(const std::shared_ptr<PhysicalDevice> &, const std::shared_ptr<Surface> &);
							const VkPresentModeKHR &Value() const;
						private:
							VkPresentModeKHR _present;
					};
			};
		}
	}

#endif
