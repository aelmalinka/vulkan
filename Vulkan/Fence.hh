/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_VULKAN_FENCE_INC
#	define ENTROPY_VULKAN_FENCE_INC

#	include "Device.hh"
#	include <limits>

	namespace Entropy
	{
		namespace Vulkan
		{
			class Fence :
				public std::enable_shared_from_this<Fence>
			{
				public:
					explicit Fence(const std::shared_ptr<Device> &);
					~Fence();
					const std::shared_ptr<Device> &getDevice() const;
					const VkFence &Handle() const;
					// 2018-09-17 AMR HACK: windows.h is causing numeric_limits to be unusable
					static constexpr std::uint64_t Forever = ULLONG_MAX;
					void Wait(const std::uint64_t = Forever);
					void Reset();
					// 2018-09-17 AMR TODO: don't copy handle every time?
					static void Wait(const std::vector<std::shared_ptr<Fence>> &, const bool = true, const std::uint64_t = Forever);
					static void Reset(const std::vector<std::shared_ptr<Fence>> &);
				private:
					std::shared_ptr<Device> _device;
					VkFence _handle;
			};
		}
	}

#endif
