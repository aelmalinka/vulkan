/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "ShaderModule.hh"
#include <fstream>

using namespace Entropy::Vulkan;
using namespace std;

ShaderModule::ShaderModule(const shared_ptr<Device> &device, const string &name) :
	_device(device),
	_handle(VK_NULL_HANDLE),
	_name(name),
	_code()
{
	// 2018-09-08 AMR TODO: does this need to be saved?
	_code = _get_code(_name);

	VkShaderModuleCreateInfo create = {};
	create.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	create.codeSize = _code.size();
	create.pCode = reinterpret_cast<const uint32_t *>(_code.data());

	EVK_SUCCESS_OR_THROW(
		vkCreateShaderModule(_device->Handle(), &create, nullptr, &_handle),
		"Failed to create Shader Module"
	);
}

ShaderModule::~ShaderModule()
{
	vkDestroyShaderModule(_device->Handle(), _handle, nullptr);
}

const shared_ptr<Device> &ShaderModule::getDevice() const
{
	return _device;
}

const VkShaderModule &ShaderModule::Handle() const
{
	return _handle;
}

const string &ShaderModule::Name() const
{
	return _name;
}

vector<char> ShaderModule::_get_code(const string &name)
{
	fstream file(name, ios::binary | ios::ate | ios::in);

	if(file.bad() || file.fail())
		ENTROPY_THROW(Exception("Failed to load Shader"));

	size_t sz = file.tellg();
	vector<char> ret(sz);

	file.seekg(0);
	file.read(ret.data(), sz);

	return ret;
}
