/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Shader.hh"

using namespace Entropy::Vulkan;
using namespace std;

Shader::Shader(
	const std::string &name,
	const shared_ptr<ShaderModule> &vertex,
	const shared_ptr<ShaderModule> &fragment,
	const shared_ptr<ShaderModule> &geometry
) :
	_name(name),
	_vertex(vertex),
	_fragment(fragment),
	_geometry(geometry),
	_info()
{
	size_t sz = 0;
	if(_vertex) sz++;
	if(_fragment) sz++;
	if(_geometry) sz++;

	_info.resize(sz);
	for(auto &create : _info) {
		create.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		create.pName = _name.c_str();
	}

	auto stage = _info.begin();

	stage->stage = VK_SHADER_STAGE_VERTEX_BIT;
	stage->module = _vertex->Handle();
	stage++;

	stage->stage = VK_SHADER_STAGE_FRAGMENT_BIT;
	stage->module = _fragment->Handle();
	stage++;

	if(_geometry) {
		stage->stage = VK_SHADER_STAGE_GEOMETRY_BIT;
		stage->module = _geometry->Handle();
		stage++;
	}
}

const string &Shader::name() const
{
	return _name;
}

const size_t Shader::size() const
{
	return _info.size();
}

const VkPipelineShaderStageCreateInfo *Shader::data() const
{
	return _info.data();
}
