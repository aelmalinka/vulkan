/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Pipeline.hh"

using namespace Entropy::Vulkan;
using namespace std;

Pipeline::Pipeline(
	const shared_ptr<Device> &device,
	const Shader &shader,
	const shared_ptr<RenderPass> &pass,
	const Pipeline::VertexInput &input,
	const Pipeline::InputAssembly &assembly,
	const Pipeline::Viewport &view,
	const Pipeline::Rasterization &rast,
	const Pipeline::Multisample &multi,
	const Pipeline::ColorBlend &blend,
	const vector<shared_ptr<DescriptorSetLayout>> &lay
) :
	_device(device),
	_pass(pass),
	_layout(device, lay),
	_handle(VK_NULL_HANDLE)
{
	VkGraphicsPipelineCreateInfo create = {};
	create.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	create.stageCount = static_cast<uint32_t>(shader.size());
	create.pStages = shader.data();
	create.pVertexInputState = &input.Info();
	create.pInputAssemblyState = &assembly.Info();
	create.pViewportState = &view.Info();
	create.pRasterizationState = &rast.Info();
	create.pMultisampleState = &multi.Info();
	create.pColorBlendState = &blend.Info();
	create.layout = _layout.Handle();
	create.renderPass = _pass->Handle();
	create.subpass = 0;
	create.basePipelineHandle = VK_NULL_HANDLE;
	create.basePipelineIndex = -1;

	EVK_SUCCESS_OR_THROW(
		vkCreateGraphicsPipelines(_device->Handle(), VK_NULL_HANDLE, 1, &create, nullptr, &_handle),
		"Failed to create Graphics Pipeline"
	);
}

Pipeline::~Pipeline()
{
	vkDestroyPipeline(_device->Handle(), _handle, nullptr);
}

const shared_ptr<Device> &Pipeline::getDevice() const
{
	return _device;
}

const VkPipeline &Pipeline::Handle() const
{
	return _handle;
}
