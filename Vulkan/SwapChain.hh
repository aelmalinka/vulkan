/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_VULKAN_SWAPCHAIN_INC
#	define ENTROPY_VULKAN_SWAPCHAIN_INC

#	include "ImageView.hh"
#	include "Semaphore.hh"

	namespace Entropy
	{
		namespace Vulkan
		{
			class SwapChain
			{
				public:
					SwapChain(const std::shared_ptr<Device> &, const std::shared_ptr<Surface> &);
					~SwapChain();
					const std::shared_ptr<Device> &getDevice() const;
					const std::shared_ptr<Surface> &getSurface() const;
					const VkSwapchainKHR &Handle() const;
					const VkExtent2D &Extent() const;
					const VkFormat &Format() const;
					std::vector<std::shared_ptr<ImageView>>::const_iterator begin() const;
					std::vector<std::shared_ptr<ImageView>>::const_iterator end() const;
					std::size_t size() const;
					// 2018-09-10 AMR TODO: this should probably not be an index
					std::uint32_t NextIndex(const Semaphore &);
				private:
					// 2018-09-07 AMR TODO: allow the user to implement these
					static VkSurfaceFormatKHR getFormats(const std::vector<Surface::Format> &);
					static VkPresentModeKHR getPresent(const std::vector<Surface::PresentMode> &);
					static VkExtent2D getExtent(const Surface::Capabilities &);
					static std::uint32_t getCount(const Surface::Capabilities &);
				private:
					std::shared_ptr<Device> _device;
					std::shared_ptr<Surface> _surface;
					// 2018-09-07 AMR TODO: there is probably a better way to store these
					VkSurfaceFormatKHR _format;
					VkPresentModeKHR _present;
					VkExtent2D _extent;
					VkSwapchainKHR _handle;
					std::vector<std::shared_ptr<ImageView>> _images;
			};
		}
	}

#endif
