/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_VULKAN_APPLICATION_INFO_INC
#	define ENTROPY_VULKAN_APPLICATION_INFO_INC

#	include <vector>
#	include "Exception.hh"

	namespace Entropy
	{
		namespace Vulkan
		{
			class ApplicationInfo
			{
				public:
					// 2018-08-28 AMR TODO: Optional Layers?
					ApplicationInfo(const char *, const std::uint32_t, std::vector<const char *> && = {}, std::vector<const char *> && = {}, std::vector<const char *> && = {}, std::vector<const char *> && = {});
					ApplicationInfo(const ApplicationInfo &);
					ApplicationInfo(ApplicationInfo &&);
					~ApplicationInfo();
					VkApplicationInfo &Handle();
					const std::vector<const char *> &Extensions() const;
					const std::vector<const char *> &Layers() const;
					static constexpr std::uint32_t Version = VK_MAKE_VERSION(0, 0, 1);
				private:
					VkApplicationInfo _handle;
					std::vector<const char *> _extensions;
					std::vector<const char *> _layers;
			};
		}
	}

#endif
