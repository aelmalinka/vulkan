/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "CommandBuffer.hh"
#include "CommandPool.hh"

using namespace Entropy::Vulkan;
using namespace std;

CommandBuffer::CommandBuffer(const shared_ptr<CommandPool> &pool, const VkCommandBuffer &handle) :
	_pool(pool),
	_handle(handle)
{}

const VkCommandBuffer &CommandBuffer::Handle() const
{
	return _handle;
}

void CommandBuffer::Begin()
{
	VkCommandBufferBeginInfo begin = {};
	begin.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	
	begin.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;

	EVK_SUCCESS_OR_THROW(
		vkBeginCommandBuffer(_handle, &begin),
		"Failed to begin command buffer"
	);
}

void CommandBuffer::End()
{
	EVK_SUCCESS_OR_THROW(
		vkEndCommandBuffer(_handle),
		"Failed to record command buffer"
	);
}

void CommandBuffer::BeginRender(const shared_ptr<Framebuffer> &frame)
{
	VkRenderPassBeginInfo begin = {};
	VkClearValue clear = {0.0f, 0.0f, 0.0f, 1.0f};
	begin.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	
	begin.renderPass = frame->getRenderPass()->Handle();
	begin.framebuffer = frame->Handle();
	begin.renderArea.offset = {0, 0};
	begin.renderArea.extent = frame->Extent();
	begin.clearValueCount = 1;
	begin.pClearValues = &clear;

	vkCmdBeginRenderPass(_handle, &begin, VK_SUBPASS_CONTENTS_INLINE);
}

void CommandBuffer::EndRender()
{
	vkCmdEndRenderPass(_handle);
}

void CommandBuffer::Bind(const shared_ptr<Pipeline> &pipe)
{
	vkCmdBindPipeline(_handle, VK_PIPELINE_BIND_POINT_GRAPHICS, pipe->Handle());
}

void CommandBuffer::Bind(const shared_ptr<Buffer> &buffer)
{
	VkDeviceSize offsets[] = {0};
	vkCmdBindVertexBuffers(_handle, 0, 1, &buffer->Handle(), offsets);
}

void CommandBuffer::Draw(const uint32_t vertices, const uint32_t instances, const uint32_t fvertex, const uint32_t finstance)
{
	vkCmdDraw(_handle, vertices, instances, fvertex, finstance);
}

// 2018-09-10 AMR TODO: WaitDstStageMask
void CommandBuffer::Submit(
	const shared_ptr<Fence> &fence,
	const vector<shared_ptr<Semaphore>> &wait,
	const vector<shared_ptr<Semaphore>> &signal
) {
	VkSubmitInfo submit = {};
	submit.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

	VkPipelineStageFlags stages[] = {
		VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT
	};

	vector<VkSemaphore> w(wait.size());
	vector<VkSemaphore> s(signal.size());

	for(auto x = 0u; x < wait.size(); x++) {
		w[x] = wait[x]->Handle();
	}
	for(auto x = 0u; x < wait.size(); x++) {
		s[x] = signal[x]->Handle();
	}

	submit.commandBufferCount = 1;
	submit.pCommandBuffers = &_handle;
	submit.waitSemaphoreCount = static_cast<uint32_t>(wait.size());
	submit.pWaitSemaphores = w.data();
	submit.pWaitDstStageMask = stages;
	submit.signalSemaphoreCount = static_cast<uint32_t>(signal.size());
	submit.pSignalSemaphores = s.data();

	VkFence f = (fence) ? fence->Handle() : VK_NULL_HANDLE;

	EVK_SUCCESS_OR_THROW(
		vkQueueSubmit(_pool->getQueue()->Handle(), 1, &submit, f),
		"Failed to submit commands"
	)
}
