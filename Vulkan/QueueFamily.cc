/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "QueueFamily.hh"
#include "PhysicalDevice.hh"

using namespace Entropy::Vulkan;
using namespace std;

QueueFamily::QueueFamily(const shared_ptr<PhysicalDevice> &d, const uint32_t i, const VkQueueFamilyProperties &p) :
	_device(d),
	_index(i),
	_properties(p)
{}

const uint32_t &QueueFamily::Index() const
{
	return _index;
}

const uint32_t &QueueFamily::Count() const
{
	return _properties.queueCount;
}

const uint32_t &QueueFamily::Flags() const
{
	return _properties.queueFlags;
}

bool QueueFamily::canPresent(const std::shared_ptr<Surface> &surface) const
{
	VkBool32 ret = false;
	vkGetPhysicalDeviceSurfaceSupportKHR(_device->Handle(), _index, surface->Handle(), &ret);
	return ret;
}
