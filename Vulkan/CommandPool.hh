/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_VULKAN_COMMANDPOOL_INC
#	define ENTROPY_VULKAN_COMMANDPOOL_INC

#	include "Queue.hh"
#	include "CommandBuffer.hh"
#	include <any>
#	include <list>

	namespace Entropy
	{
		namespace Vulkan
		{
			class CommandPool :
				public std::enable_shared_from_this<CommandPool>
			{
				public:
					template<typename T>
					class Allocator
					{
						public:
							using value_type = T;
							using propagate_on_container_move_assignment = std::true_type;
							using propagate_on_container_copy_assignment = std::true_type;
							using propagate_on_container_swap = std::true_type;
						public:
							Allocator() noexcept;
							Allocator(const std::shared_ptr<CommandPool> &, const VkCommandBufferLevel) noexcept;
							template<typename U> Allocator(const Allocator<U> &) noexcept;
							const std::shared_ptr<CommandPool> &getPool() const;
							const VkCommandBufferLevel &getLevel() const;
							T *allocate(std::size_t);
							void deallocate(T *, std::size_t) noexcept;
							template<typename U, typename ...Args > void construct(U *, Args && ...);
							// 2018-09-23 AMR TODO: construct multiple CommandBuffers in one go
							template<typename ...Args> void construct(CommandBuffer *, Args && ...);
							// 2018-09-23 AMR TODO: is there really no way to destroy CommandBuffers?
						private:
							std::shared_ptr<CommandPool> _pool;
							VkCommandBufferLevel _level;
					};
				public:
					// 2018-09-23 AMR TODO: variable definition for container_type
					using allocator_type = Allocator<CommandBuffer>;
				public:
					CommandPool(const std::shared_ptr<Device> &, const std::shared_ptr<Queue> &);
					~CommandPool();
					const std::shared_ptr<Device> &getDevice() const;
					const std::shared_ptr<Queue> &getQueue() const;
					const VkCommandPool &Handle() const;
					template<typename C = std::vector<CommandBuffer, allocator_type>>
					C get(const VkCommandBufferLevel = VK_COMMAND_BUFFER_LEVEL_PRIMARY);
				private:
					std::shared_ptr<Device> _device;
					std::shared_ptr<Queue> _queue;
					VkCommandPool _handle;
					std::list<std::any> _allocs;
			};
		}
	}

#	include "CommandPool.impl.hh"


#endif
