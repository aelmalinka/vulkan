/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_VULKAN_EVENT_INC
#	define ENTROPY_VULKAN_EVENT_INC

#	include "Exception.hh"
#	include <Entropy/Event.hh>

	namespace Entropy
	{
		namespace Vulkan
		{
			class Event :
				public Entropy::Event
			{
				public:
					explicit Event(const std::size_t &);
					virtual ~Event();
					// 2018-08-28 AMR TODO: Automate, determine location in events (in place of Theia, after Theia? as Theia?)
					static constexpr std::size_t First = 100;
					static constexpr std::size_t Last = First + 10;
			};
		}
	}

#endif
