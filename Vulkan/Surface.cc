/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Surface.hh"
#include "PhysicalDevice.hh"
#include <GLFW/glfw3.h>

using namespace Entropy::Vulkan;
using namespace std;

namespace Entropy {
	namespace Vulkan {
		template<>
		Surface::Surface(const shared_ptr<Instance> &instance, GLFWwindow *window) :
			_instance(instance),
			_handle(VK_NULL_HANDLE)
		{
			EVK_SUCCESS_OR_THROW(
				glfwCreateWindowSurface(_instance->Handle(), window, nullptr, &_handle),
				"Failed to create Surface"
			);
		}
	}
}

Surface::~Surface()
{
	vkDestroySurfaceKHR(_instance->Handle(), _handle, nullptr);
}

const VkSurfaceKHR &Surface::Handle() const
{
	return _handle;
}

const shared_ptr<Instance> &Surface::getInstance() const
{
	return _instance;
}

Surface::Capabilities::Capabilities(const shared_ptr<PhysicalDevice> &d, const shared_ptr<Surface> &s) :
	_caps(),
	_surface(s)
{
	EVK_SUCCESS_OR_THROW(
		vkGetPhysicalDeviceSurfaceCapabilitiesKHR(d->Handle(), s->Handle(), &_caps),
		"Failed to get Device/Surface Capabilities"
	);
}

const shared_ptr<Surface> &Surface::Capabilities::getSurface() const
{
	return _surface;
}

const VkSurfaceTransformFlagBitsKHR &Surface::Capabilities::CurrentTransform() const
{
	return _caps.currentTransform;
}

const uint32_t &Surface::Capabilities::MinImageCount() const
{
	return _caps.minImageCount;
}

const uint32_t &Surface::Capabilities::MaxImageCount() const
{
	return _caps.maxImageCount;
}

const VkExtent2D &Surface::Capabilities::CurrentExtent() const
{
	return _caps.currentExtent;
}

const VkExtent2D &Surface::Capabilities::MinExtent() const
{
	return _caps.minImageExtent;
}

const VkExtent2D &Surface::Capabilities::MaxExtent() const
{
	return _caps.maxImageExtent;
}

Surface::Format::Format(const VkSurfaceFormatKHR &f) :
	_format(f)
{}

const VkFormat &Surface::Format::Pixels() const
{
	return _format.format;
}

const VkColorSpaceKHR &Surface::Format::ColorSpace() const
{
	return _format.colorSpace;
}

const VkSurfaceFormatKHR &Surface::Format::Value() const
{
	return _format;
}

// 2018-09-04 AMR TODO: Templatify to DRY
vector<Surface::Format> Surface::Format::Get(const shared_ptr<PhysicalDevice> &phys, const shared_ptr<Surface> &surface)
{
	vector<Surface::Format> ret;
	uint32_t count = 0;
	EVK_SUCCESS_OR_THROW(
		vkGetPhysicalDeviceSurfaceFormatsKHR(phys->Handle(), surface->Handle(), &count, nullptr),
		"Failed to get Device/Surface Formats"
	);
	vector<VkSurfaceFormatKHR> formats(count);
	ret.reserve(count);
	EVK_SUCCESS_OR_THROW(
		vkGetPhysicalDeviceSurfaceFormatsKHR(phys->Handle(), surface->Handle(), &count, formats.data()),
		"Failed to get Device/Surface Formats"
	);
	for(const auto &f : formats) {
		ret.emplace_back(f);
	}

	return ret;
}

Surface::PresentMode::PresentMode(const VkPresentModeKHR &p) :
	_present(p)
{}

const VkPresentModeKHR &Surface::PresentMode::Value() const
{
	return _present;
}

vector<Surface::PresentMode> Surface::PresentMode::Get(const shared_ptr<PhysicalDevice> &phys, const shared_ptr<Surface> &surface)
{
	vector<Surface::PresentMode> ret;
	uint32_t count = 0;
	EVK_SUCCESS_OR_THROW(
		vkGetPhysicalDeviceSurfacePresentModesKHR(phys->Handle(), surface->Handle(), &count, nullptr),
		"Failed to get Device/Surface Formats"
	);
	vector<VkPresentModeKHR> modes(count);
	ret.reserve(count);
	EVK_SUCCESS_OR_THROW(
		vkGetPhysicalDeviceSurfacePresentModesKHR(phys->Handle(), surface->Handle(), &count, modes.data()),
		"Failed to get Device/Surface Formats"
	);
	for(const auto &f : modes) {
		ret.emplace_back(f);
	}

	return ret;
}
