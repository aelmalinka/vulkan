/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_VULKAN_QUEUE_FAMILY_INC
#	define ENTROPY_VULKAN_QUEUE_FAMILY_INC

#	include "Surface.hh"

	namespace Entropy
	{
		namespace Vulkan
		{
			class PhysicalDevice;

			class QueueFamily
			{
				public:
					QueueFamily(const std::shared_ptr<PhysicalDevice> &, const std::uint32_t, const VkQueueFamilyProperties &);
					const std::uint32_t &Index() const;
					const std::uint32_t &Count() const;
					const std::uint32_t &Flags() const;
					bool canPresent(const std::shared_ptr<Surface> &) const;
				private:
					std::shared_ptr<PhysicalDevice> _device;
					std::uint32_t _index;
					VkQueueFamilyProperties _properties;
			};
		}
	}

#endif
